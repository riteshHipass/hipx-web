Application.$controller("IntegrationServicesPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        //Check for Integration Id
        if ($scope.pageParams.integrationId === undefined || $scope.pageParams.integrationId === "") {
            window.history.back();
        }
    };


    $scope.integrationServiceListTableBeforedatarender = function($isolateScope, $data, $columns) {
        $isolateScope.gridOptions.data = undefined;
    };



    $scope.integrationServiceListTableDatarender = function($isolateScope, $data) {
        var totalRecords = $scope.Widgets.integrationServiceListTable.dataNavigator.dataSize;
        var maxResults = $scope.Widgets.integrationServiceListTable.dataNavigator.maxResults;
        var currentPage = $scope.Widgets.integrationServiceListTable.dataNavigator.dn.currentPage;

        $scope.paginationPrefix = $scope.$root.getPaginationPrefix(totalRecords, maxResults, currentPage);
    };

}]);


Application.$controller("integrationServiceListTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.addNewRowAction = function($event) {
            $scope.Variables.stvEditIntegrationService.dataSet = {};
        };


        $scope.updaterowAction = function($event, $rowData) {
            $scope.Variables.stvEditIntegrationService.dataSet = $rowData;
            $scope.Variables.stvCreateIntegrationService.dataSet = $rowData;
        };

    }
]);

Application.$controller("integrationServicesDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.saveButtonClick = function($event, $isolateScope) {
            $scope.Widgets.createIntegrationServiceForm.submit();
        };



        $scope.createIntegrationServiceFormSubmit = function($event, $isolateScope, $formData) {
            // Create Integration Service
            if ($scope.Variables.stvEditIntegrationService.dataSet.integrationServiceId === undefined) {
                $scope.Variables.createIntegrationService.setInput('RequestBody', $scope.Variables.stvCreateIntegrationService.dataSet);
                $scope.Variables.createIntegrationService.invoke();
            } else {
                //Update Integration Service
                $scope.Variables.editIntegrationService.setInput('RequestBody', processContactData($scope.Variables.stvCreateIntegrationService.dataSet));
                $scope.Variables.editIntegrationService.invoke();
            }

        };

        // Method Process Data before insert
        function processContactData(service) {
            if (service.hipIntegration !== undefined || service.hipIntegration !== null) {
                delete service.hipIntegration;
            }
            return service;
        }


    }
]);