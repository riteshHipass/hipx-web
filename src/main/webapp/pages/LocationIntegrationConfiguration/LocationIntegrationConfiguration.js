Application.$controller("LocationIntegrationConfigurationPageController", ["$scope", "NavigationService", function($scope, NavigationService) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };


    $scope.locationIntegrationConfigTableBeforedatarender = function($isolateScope, $data, $columns) {
        $isolateScope.gridOptions.data = undefined;
    };


    $scope.breadcrumbBeforenavigate = function($isolateScope, $item) {
        NavigationService.goToPage($item.id, {
            'urlParams': $scope.pageParams
        });
        return false;
    };


    $scope.locationIntegrationConfigTableDatarender = function($isolateScope, $data) {
        var totalRecords = $scope.Widgets.locationIntegrationConfigTable.dataNavigator.dataSize;
        var maxResults = $scope.Widgets.locationIntegrationConfigTable.dataNavigator.maxResults;
        var currentPage = $scope.Widgets.locationIntegrationConfigTable.dataNavigator.dn.currentPage;

        $scope.paginationPrefix = $scope.$root.getPaginationPrefix(totalRecords, maxResults, currentPage);
    };

}]);


Application.$controller("locationIntegrationConfigTableController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.addNewRowAction = function($event) {
            $scope.Variables.stvEditIntegrationConfig.dataSet = {};
        };

        $scope.updaterowAction = function($event, $rowData) {
            $scope.Variables.stvEditIntegrationConfig.dataSet = $rowData;
            $scope.Variables.stvCreateIntegrationConfig.dataSet = $rowData;
        };

    }
]);

Application.$controller("locationIntegrationConfigDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.integrationConfigFormSubmit = function($event, $isolateScope, $formData) {
            // Create Config
            if ($scope.Variables.stvEditIntegrationConfig.dataSet.locationIntegrationConfigId === undefined) {
                $scope.Variables.createClientLocationIntegrationConfig.setInput('RequestBody', $scope.Variables.stvCreateIntegrationConfig.dataSet);
                $scope.Variables.createClientLocationIntegrationConfig.invoke();
            } else {
                //Update Config
                $scope.Variables.editClientLocationIntegrationConfig.setInput('RequestBody', processContactData($scope.Variables.stvCreateIntegrationConfig.dataSet));
                $scope.Variables.editClientLocationIntegrationConfig.invoke();
            }
        };


        $scope.saveButtonClick = function($event, $isolateScope) {
            $scope.Widgets.integrationConfigForm.submit();
        };

        // Method Process Data before insert
        function processContactData(config) {
            if (config.hipIntegrationServiceList !== undefined || config.hipIntegrationServiceList !== null) {
                delete config.hipIntegrationServiceList;
            }
            if (config.hipClientLocation !== undefined || config.hipClientLocation !== null) {
                delete config.hipClientLocation;
            }
            return config;
        }

    }
]);