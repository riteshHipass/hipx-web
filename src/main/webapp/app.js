Application.run(function($rootScope) {
    "use strict";
    /* perform any action on the variables within this block(on-page-load) */
    $rootScope.onAppVariablesReady = function() {
        /*
         * variables can be accessed through '$rootScope.Variables' property here
         * e.g. $rootScope.Variables.staticVariable1.getData()
         */
    };

    /* perform any action on session timeout here, e.g clearing some data, etc */
    $rootScope.onSessionTimeout = function() {
        /*
         * NOTE:
         * On re-login after session timeout:
         * if the same user logs in(through login dialog), app will retain its state
         * if a different user logs in, app will be reloaded and user is redirected to respective landing page configured in Security.
         */
    };

    /*
     * This application level callback function will be invoked after the invocation of PAGE level onPageReady function.
     * Use this function to write common logic across the pages in the application.
     * activePageName : name of the page
     * activePageScope: scope of the page
     * $activePageEl  : page jQuery element
     */
    $rootScope.onPageReady = function(activePageName, activePageScope, $activePageEl) {

    };

    /*
     * This application level callback function will be invoked after a Variable receives an error from the target service.
     * Use this function to write common error handling logic across the application.
     * source:      Variable object or Widget Scope
     * errorMsg:    The error message returned by the target service. This message will be displayed through appNotification variable
     *              You can change this though $rootScope.Variables.appNotification.setMessage(YOUR_CUSTOM_MESSAGE)
     * xhrObj:      The xhrObject used to make the service call
     *              This object contains useful information like statusCode, url, request/response body.
     */
    $rootScope.onServiceError = function(source, errorMsg, xhrObj) {

    };

    $rootScope.formatPhoneNumber = function(phoneNumber) {
        var cleaned = ("" + phoneNumber).replace(/\D/g, '');
        var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
        return (!match) ? null : "(" + match[1] + ") " + match[2] + "-" + match[3];
    };

    $rootScope.getPaginationPrefix = function(totalRecords, maxResults, currentPage) {
        var min, max, total = 0;
        if (maxResults >= totalRecords) {
            return "";
        } else {
            if (currentPage * maxResults <= totalRecords) {
                max = currentPage * maxResults;
            } else {
                max = totalRecords;
            }
            min = ((currentPage - 1) * maxResults) + 1;

            return min + "-" + max + " of " + totalRecords + " Records";
        }

    }

});

Application.directive('hipAddress', function($timeout, $rootScope) {

    function formatPhoneNumber(scope) {
        var phoneNumber = scope.variable.phoneNumber;
        var cleaned = ("" + phoneNumber).replace(/\D/g, '');
        var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
        return (!match) ? null : "(" + match[1] + ") " + match[2] + "-" + match[3];
    };

    return {
        restrict: 'E',
        scope: {
            variable: '=',
        },
        link: function(scope) {
            scope.formatPhoneNumber = formatPhoneNumber.bind(undefined, scope);
        },
        template: '<p class="address-line" >Phone: {{formatPhoneNumber() }} </p>' +
            '<p class="address-line" >Fax: {{variable.faxNumber}}</p>' +
            '<p class="address-line" >Email: {{variable.emailId}}</p>'
    };
});